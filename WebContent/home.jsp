<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
		<title>社内掲示板</title>
		<link href="./css/homestyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div class="header">
		<h1>ABC.corporation</h1>
		<h2 class="h2-sticky">Hello! ${user.name}さん</h2>
		<ul id="nav">
		<li><a href="newPost">新規投稿</a>
		<c:if test= "${user.branchcode==0 && user.departmentcode==0 }">
		<li><a href="managementuser">ユーザー管理</a></li>
		</c:if>
		<li><a href="logout">ログアウト</a></li>
		</ul>
	</div>

	<div class="search">
		<h5>検索機能</h5>
		<form action="index.jsp" method="get">
			<p><label for="keyCategory">カテゴリ</label>
					<input name="keyCategory" value = "${keyCategory}">
				<label for="startDate">開始日</label>
					<input name="startDate" value="${startDate}" id="startDatePicker">
				<label for="endDate">終了日</label>
					<input name="endDate" value="${endDate}" id="endDatePicker"> <br/>
					<input type="submit" value="検索" >
					<input type="button" value="リセット" onClick="location.href='./'">
					<input type="hidden" name="searchflag" value="on">
			</p>
		</form>
	</div>

		<!-- エラーメッセージの表示 -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="main-contents">
		<div class="home">
			<c:forEach items="${posts}" var="post">
				<!-- 投稿一覧 -->
				<div class="post">
					<div class="title">
						<h5>件名</h5>
							<span class="title">
							<c:out value="${post.title}" /></span> <br/>
					</div>
					<div class="name">
						<h5>名前</h5>
						<span class="name">
						<c:out value="${post.name}" /></span> <br/>
					</div>
					<div class="category">
						<h5>カテゴリ</h5>
						<span class="category">
						<c:out value="${post.category}" /></span> <br/>
					</div>
					<div class="text">
						<h5>本文</h5>
						<!-- 物理的にｂｒタグを意味するコード（改行させる） -->
						<c:forEach var="s" items="${fn:split(post.text, '
						')}">
						<c:out value="${s}" /> <br/>
						</c:forEach>
					</div>
					<div class="date">
						<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<!-- 投稿削除 -->
					<c:if test="${user.userid == post.userId }">
						<div class = delete>
							<form action="index.jsp" method="post">
								<input type="submit" value="投稿削除"onClick= "return check();">
								<input type="hidden" name="editid" value="${post.editid}">
								<input type="hidden" name="deleteflag" value="on">
							</form>
						</div>
					</c:if>
					<!-- コメントの登録 -->
					<div class="form-area">
						<form action="index.jsp" method="post"> <br />
							<label for="comment">コメント</label>
							<textarea name="comment" cols="10" rows="10" class="tweet-box">${text}</textarea> <br />
							<input type="hidden" name="editid" value="${post.editid}">
							<input type="submit" value="コメントする">（500文字まで）<br />
							<input type="hidden" name="commentflag" value="on">
						</form>
					</div>
					<!-- コメント一覧表示 -->
					<div class="comment-show">
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
								<c:if test="${post.editid == comment.editid }">
									<div class="date">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<!-- 物理的にｂｒタグを意味するコード（改行させる） -->
									<c:forEach var="s" items="${fn:split(comment.text, '
									')}">
										<c:out value="${s}" /><br/>
									</c:forEach>
									<div class="name">
										<span class="name">
										<c:out value="${comment.name}" /></span> <br/>
									</div>
										<!-- コメント削除 -->
										<c:if test="${user.userid == comment.userid }">
											<div class = comment-delete>
												<form action="./" method="post">
													<input type="submit" value="コメント削除"onClick= "return check();">
													<input type="hidden" name="commentid" value="${comment.commentid}">
													<input type="hidden" name="commentDeleteFlag" value="on">
												</form>
											</div>
										</c:if>
								</c:if>
							</div>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)TakeyaManabu</div>
       </div>
        <script>
				function check(){
					if(!confirm('本当に削除しますか？')){
						return false;
					}else{
						return true;
					}
				}

				$(function check(){
					$('#startDatePicker').datepicker({
					});
				});

				$(function check(){
					$('#endDatePicker').datepicker({
					});
				});
			</script>
	</body>
</html>
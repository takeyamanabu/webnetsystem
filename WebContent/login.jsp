<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link href="./css/loginstyle.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<div class="header">
			<h1>ABC.corporation</h1>
		</div>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<div class = "field">
				<h2>Login</h2>
				<form action="login" method="post"> <br />
					<label for="loginid">ログインＩＤ</label>
						<input name="loginid" id="loginid" type= "text"value= "${loginid}" /> <br />
					<label for="password">パスワード</label>
						<input name="password" id="password" type="password" /> <br />
					<input type="submit" value="Enter" /> <br />
				</form>
				</div>
				<div class="copyright">Copyright(c)TakeyaManabu</div>
	</body>
</html>
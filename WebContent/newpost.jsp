<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/newpoststyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1>新規投稿</h1>
		<!-- エラーメッセージの表示 -->
		<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		<a href = "./">ホーム</a>
		<div class="form-area">
			<form action="newPost" method="post"> <br />
			<label for="title">件名  (30文字まで)</label>
				<input name="title" id="title" value="${post.title}"/> <br />
			<label for="category">カテゴリ  (10文字まで)</label>
				<input name="category" id="category"value="${post.category}"/> <br />
			<label for="text">本文  (1000文字まで)</label>
				<textarea name="text" cols="50" rows="30" class="tweet-box">${post.text}</textarea> <br />
			<input type="submit" value="投稿"><br />
			</form>
		</div>
		<div class ="return">
			<a href = "./">戻る</a>
		</div>
	</body>
</html>
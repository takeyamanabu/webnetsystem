<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<link href="./css/signupstyle.css" rel="stylesheet" type="text/css">
		<title>ユーザー登録</title>
	</head>
	<body>
		<div class="header">
			<h1>ユーザー新規登録</h1>
		</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
				<form action="signup" method="post"> <br />
					<label for="loginid">ログインＩＤ</label>
						<input name="loginid" id="loginid" value="${user.loginid}" /> <br />

					<label for="password">パスワード</label>
						<input name="password" type="password" id="password"value="${user.password}" /> <br />

					<label for="confirm">パスワード(確認用)</label>
						<input name="confirm" type="password" id="confirm" value="${user.confirm}" /> <br />

					<label for="name">ユーザー名 </label>
						<input name="name" id="name" value="${user.name}" /> <br />
					<!-- プルダウンリストの作成-->
					<label for="branch_code">所属名</label>
						<select name="branchcode" id="branchcode">
							<c:if test = "${user.branchcode == 0}">
								<option value="0" selected>本社</option>
						  		<option value="1">東京支店</option>
						  		<option value="2">大阪支店</option>
						  		<option value="3">福岡支店</option>
							</c:if>
							<c:if test = "${user.branchcode == 1}">
								<option value="0">本社</option>
						  		<option value="1"selected>東京支店</option>
						  		<option value="2">大阪支店</option>
						  		<option value="3">福岡支店</option>
							</c:if>
							<c:if test = "${user.branchcode == 2}">
								<option value="0">本社</option>
						  		<option value="1">東京支店</option>
						  		<option value="2"selected>大阪支店</option>
						  		<option value="3">福岡支店</option>
							</c:if>
							<c:if test = "${user.branchcode == 3}">
								<option value="0" >本社</option>
						  		<option value="1">東京支店</option>
						  		<option value="2">大阪支店</option>
						  		<option value="3"selected>福岡支店</option>
							</c:if>
							<c:if test = "${empty user.branchcode}">
								<option value="0">本社</option>
								<option value="1">東京支店</option>
								<option value="2">大阪支店</option>
								<option value="3">福岡支店</option>
							</c:if>
						</select><br />

					<label for="department_code">部署名</label>
						<select name="departmentcode" id="departmentcode">
							<c:if test = "${user.departmentcode == 0}">
								<option value="0"selected>総務部</option>
								<option value="1">営業部</option>
								<option value="2">販売部</option>
								<option value="3">人事部</option>
								<option value="4">開発部</option>
							</c:if>
							<c:if test = "${user.departmentcode == 1}">
								<option value="0">総務部</option>
								<option value="1"selected>営業部</option>
								<option value="2">販売部</option>
								<option value="3">人事部</option>
								<option value="4">開発部</option>
							</c:if>
							<c:if test = "${user.departmentcode == 2}">
								<option value="0">総務部</option>
								<option value="1">営業部</option>
								<option value="2"selected>販売部</option>
								<option value="3">人事部</option>
								<option value="4">開発部</option>
							</c:if>
							<c:if test = "${user.departmentcode == 3}">
								<option value="0">総務部</option>
								<option value="1">営業部</option>
								<option value="2">販売部</option>
								<option value="3"selected>人事部</option>
								<option value="4">開発部</option>
							</c:if>
							<c:if test = "${user.departmentcode == 4}">
								<option value="0">総務部</option>
								<option value="1">営業部</option>
								<option value="2">販売部</option>
								<option value="3">人事部</option>
								<option value="4"selected>開発部</option>
							</c:if>
							<c:if test = "${empty user.departmentcode}">
								<option value="0">総務部</option>
								<option value="1">営業部</option>
								<option value="2">販売部</option>
								<option value="3">人事部</option>
								<option value="4">開発部</option>
							</c:if>
						</select><br />
					<input type="submit" value="登録" /> <br />
				</form>
			</div>
			<ul id="bottom-link">
				<li><a href="managementuser">管理画面へ戻る</a></li>
			</ul>
	</body>
</html>
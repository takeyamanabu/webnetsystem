package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;
@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	//POST送信はデータに変更を加える（登録など）
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = getUser(request);

		if (isValid(request, messages) == true) {
			//Beansに格納したユーザ情報を登録する
			new UserService().register(user);
			response.sendRedirect("./managementuser");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}


	private User getUser(HttpServletRequest request)
	        throws IOException, ServletException {

		//Beansクラスでユーザー情報をまとめる
		User user = new User();
		user.setLoginid(request.getParameter("loginid"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setConfirm(request.getParameter("confirm"));
		user.setBranchcode(Integer.parseInt(request.getParameter("branchcode")));
		user.setDepartmentcode(Integer.parseInt(request.getParameter("departmentcode")));
	    return user;
    }

	//Validationを実装する

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String confirm = request.getParameter("confirm");
		String name = request.getParameter("name");
		String branchcode = request.getParameter("branchcode");
		String departmentcode = request.getParameter("departmentcode");

		//ログインＩＤに関するエラーメッセージ
		if (StringUtils.isBlank(loginid) == true) {
		    messages.add("ログインＩＤを入力してください");
		}else if(loginid.getBytes().length <= 5){
			messages.add("ログインＩＤは6文字以上で入力してください");
		}else if(loginid.getBytes().length >= 21 ){
			messages.add("ログインＩＤは20文字以内で入力してください");
		}else if(!loginid.matches("^[0-9a-zA-Z]+$")){
			messages.add("ログインＩＤは半角英数字のみ有効です");
			//ログインＩＤの重複をチェックする
		}else if(!new UserService(). getLoginId(loginid)){
			messages.add("ログインＩＤが重複しています");
		}

		//パスワードに関するエラーメッセージ
		if (StringUtils.isBlank(password) == true || StringUtils.isBlank(confirm) == true) {
		    messages.add("パスワードを入力してください");
		}else if(password.getBytes().length <= 5){
			messages.add("パスワードは6文字以上で入力してください");
		}else if(password.getBytes().length >= 21 ){
			messages.add("パスワードは20文字以内で入力してください");
		}else if(!password.matches("^[0-9a-zA-Z -/:-@\\[-\\`\\{-\\~]+$")){
			messages.add("パスワードは記号を含む全ての半角文字のみ有効です");
		}

		if (StringUtils.equals(password,confirm)== false) {
			messages.add("確認用パスワードが一致しません");
		}
		//ユーザ名に関するエラーメッセージ
		if (StringUtils.isBlank(name) == true) {
		    messages.add("ユーザー名を入力してください");
		}else if(name.length() >=11){
			messages.add("名前は10文字以内で入力してください");
		}

		//支店・部署コードに関するエラー
		if (!branchcode.matches("^[0-3]$")) {
		    messages.add("支店コードを選択してください");
		}
		if (!departmentcode.matches("^[0-4]$")) {
		    messages.add("部署・役職コードを選択してください");
		}
		if (messages.size() == 0) {
		    return true;
		} else {
		    return false;
		}
    }

}

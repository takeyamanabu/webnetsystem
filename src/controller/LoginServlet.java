package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/login.jsp").forward(request,response);
	}

	//ログイン機能の実装
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(loginid, password);

		HttpSession session = request.getSession();
		//ユーザが存在する且つ、アカウントが生きてる場合、ホームへ遷移
		if (user != null ) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}else {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginid", loginid);
			request.setAttribute("password", password);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
	}

}

package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Post;
import beans.User;
import beans.UserPost;
import service.CommentService;
import service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		 Calendar cal = Calendar.getInstance();
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		 cal.add(Calendar.DATE,1);
		 //抽出条件なしのデフォルトの値
		 String keyCategory = "";
		 String startDate ="2018/04/01";
		 String endDate =sdf.format(cal.getTime());
		//検索の入力値を取る
		if(request.getParameter("searchflag")!=null){

			if(!request.getParameter("keyCategory").equals("")){
				keyCategory = request.getParameter("keyCategory");
			}
			if(!request.getParameter("startDate").equals("")){
				startDate = request.getParameter("startDate");
			}
			if(!request.getParameter("endDate").equals("")){
				endDate = (request.getParameter("endDate"));
			}
			request.setAttribute("errorMessages", messages);
			request.setAttribute("keyCategory", keyCategory);
			request.setAttribute("startDate", startDate);
			request.setAttribute("endDate",  endDate);
		}
		//検索ボタンが押さなければ抽出条件はデフォルト（全て表示）

		List<UserPost> posts = new PostService().getPost(keyCategory,startDate,endDate);
		List<Comment> comments = new CommentService().getComment();
		User user = (User) request.getSession().getAttribute("loginUser");


		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("user", user);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");


		//投稿の削除機能
		if(request.getParameter("deleteflag")!=null){
			Post post = new Post();
			post.setEditid(Integer.parseInt(request.getParameter("editid")));
			new PostService().delete(post);
			response.sendRedirect("./");
		}

		//コメントの削除機能
		if(request.getParameter("commentDeleteFlag")!=null){
			Comment comment = new Comment();
			comment.setCommentid(Integer.parseInt(request.getParameter("commentid")));
			new CommentService().delete(comment);
			response.sendRedirect("./");
		}


		//コメントを登録する機能
		if(request.getParameter("commentflag")!=null){
			if (isValid(request, messages) == true) {
				Comment comment = new Comment();
				comment.setEditid(Integer.parseInt(request.getParameter("editid")));
				comment.setText(request.getParameter("comment"));
				comment.setUserid(user.getUserid());

				new CommentService().register(comment);

				response.sendRedirect("./");

			} else {
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
			}
		}

	}

	//コメントのバリデーション機能
	private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("comment");

        if (StringUtils.isBlank(text) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < text.length()) {
            messages.add("500文字以内で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
public class UserDao {

	//ユーザー新規登録のINSERT文
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_code");
            sql.append(", department_code");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_code
            sql.append(", ?"); // department_code
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginid());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchcode());
            ps.setInt(5, user.getDepartmentcode());
            //実行コマンド
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    //ユーザー編集のＵＰＤＡＴＥ文
    public void update(Connection connection, User user) {

    	PreparedStatement ps = null;
    	try{
			StringBuilder sql = new StringBuilder();
			//パスワードが空欄の場合、パスワードは変更しない
			if(!StringUtils.isBlank(user.getPassword())){
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", password = ?");
				sql.append(", name = ?");
				sql.append(", branch_code = ?");
				sql.append(", department_code = ?");
				sql.append(", updated_date = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" user_id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginid());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchcode());
				ps.setInt(5, user.getDepartmentcode());
				ps.setInt(6, user.getUserid());

    		}else{
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", name = ?");
				sql.append(", branch_code = ?");
				sql.append(", department_code = ?");
				sql.append(", updated_date = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" user_id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginid());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchcode());
				ps.setInt(4, user.getDepartmentcode());
				ps.setInt(5, user.getUserid());
    		}
			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
            }

    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
    }

    //アカウントの停止再開ＵＰＤＡＴＥ文
    public void updateOnOff(Connection connection, User user) {

    	PreparedStatement ps = null;
    	try{
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE users SET");
    		sql.append("  is_stopped = ?");
    		sql.append(" WHERE");
    		sql.append(" user_id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, user.getIsStopped());
            ps.setInt(2, user.getUserid());
            int count = ps.executeUpdate();
            if(count == 0) {
            	throw new NoRowsUpdatedRuntimeException();
            }

    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
    }

    //ユーザ編集画面でユーザ情報の既定値を取り出す
    public User getUser(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE user_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    //ユーザー情報からログインＩＤとパスワードを参照する
    public User getUser(Connection connection, String loginid,String password) {

    	PreparedStatement ps = null;
    	try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_stopped = 0";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginid);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //登録画面で、ログインＩＤに重複があればＴＲＵＥを返す
    public boolean loginIdChecker(Connection connection, String loginid){
    	PreparedStatement ps = null;
    	try {
            String sql = "SELECT * FROM users WHERE login_id = ? ";

            ps = connection.prepareStatement(sql);

            ps.setString(1, loginid);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return true;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

  //ユーザ編集画面で、ログインＩＤに重複があればＴＲＵＥを返す

    public boolean editLoginIdChecker(Connection connection, String loginid, int userid){
    	PreparedStatement ps = null;
    	try {
    		//編集しているユーザーのログインＩＤが該当しないようにする
            String sql = "SELECT * FROM users WHERE login_id = ? AND user_id <> ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, loginid);
            ps.setInt(2, userid);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return true;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    //ユーザ情報一覧をリストアップする
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int userid = rs.getInt("user_id");
                String loginid = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branchcode = rs.getInt("branch_code");
                int departmentcode = rs.getInt("department_code");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setUserid(userid);
                user.setLoginid(loginid);
                user.setPassword(password);
                user.setName(name);
                user.setBranchcode(branchcode);
                user.setDepartmentcode(departmentcode);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }

    }
}



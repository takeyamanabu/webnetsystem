package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserDetail;
import exception.SQLRuntimeException;

public class UserDetailDao {

	public List<UserDetail> getUserDetails(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("branches.branch_name as branch_name, ");
			sql.append("departments.department_name as department_name, ");
			sql.append("users.is_stopped as is_stopped, ");
			sql.append("users.branch_code as branch_code, ");
			sql.append("users.department_code as department_code ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ON users.branch_code = branches.branch_code ");
			sql.append("INNER JOIN departments ON users.department_code = departments.department_code");


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserDetail> ret = toUserDetailList(rs);
			return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	private List<UserDetail> toUserDetailList(ResultSet rs)
			throws SQLException {

		List<UserDetail> ret = new ArrayList<UserDetail>();
		try {
			while (rs.next()) {
				int userId = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String branchName = rs.getString("branch_name");
				String departmentName = rs.getString("department_name");
				int isStopped = rs.getInt("is_stopped");
				int branchCode = rs.getInt("branch_code");
				int departmentCode = rs.getInt("department_code");

				UserDetail detail = new UserDetail();

				detail.setUserId(userId);
				detail.setLoginId(loginId);
				detail.setName(name);
				detail.setBranchName(branchName);
				detail.setDepartmentName(departmentName);
				detail.setIsStopped(isStopped);
				detail.setBranchCode(branchCode);
				detail.setDepartmentCode(departmentCode);

				ret.add(detail);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}

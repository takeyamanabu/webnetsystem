package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("edit_id");
            sql.append(", text");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // editid
            sql.append(", ?"); // text
            sql.append(", ?"); // userid
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getEditid());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getUserid());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	//コメントの削除を行うDELETE文
		 public void delete(Connection connection, Comment comment){

			 PreparedStatement ps = null;
			 try {
				 StringBuilder sql = new StringBuilder();
				 sql.append("DELETE FROM comments");
				 sql.append(" WHERE comment_id = ?");

				 ps = connection.prepareStatement(sql.toString());

				 ps.setInt(1, comment.getCommentid());
				 ps.executeUpdate();

			 } catch (SQLException e) {
				 throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
		 }
}

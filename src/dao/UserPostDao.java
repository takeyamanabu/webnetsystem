package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;
public class UserPostDao {

	//投稿一覧表示のための表結合
	public List<UserPost> getUserPosts(Connection connection, int num, String keyCategory, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT ");
				sql.append("posts.title as title, ");
				sql.append("posts.category as category, ");
				sql.append("posts.text as text, ");
				sql.append("posts.edit_id as edit_id, ");
				sql.append("posts.user_id as user_id, ");
				sql.append("users.name as name, ");
				sql.append("posts.created_date as created_date ");
				sql.append("FROM posts ");
				sql.append("INNER JOIN users ");
				sql.append("ON posts.user_id = users.user_id ");
				//日付の期間で絞込み
				sql.append("WHERE posts.created_date ");
				sql.append("BETWEEN ? ");
				sql.append("AND ? ");
				sql.append("AND category ");
				sql.append("LIKE ? ");
				sql.append("ORDER BY created_date DESC limit " + num);

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, startDate);
				ps.setString(2, endDate +" 23:59:59");
				ps.setString(3, "%" + keyCategory + "%");

				ResultSet rs = ps.executeQuery();
				List<UserPost> ret = toUserPostList(rs);
				return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String name = rs.getString("name");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				int editid = rs.getInt("edit_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserPost post = new UserPost();
				post.setTitle(title);
				post.setName(name);
				post.setText(text);
				post.setCategory(category);
				post.setUserId(userId);
				post.setEditId(editid);
				post.setCreated_date(createdDate);

				ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}




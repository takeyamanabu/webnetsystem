package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;
import dao.UserCommentDao;
public class CommentService {

	//コメントを登録するためのメソッド
	public void register(Comment comment) {

		Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	//コメント削除のサービス
	public void delete(Comment comment) {

		Connection connection = null;

		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//コメント一覧を表示するメソッド

	private static final int LIMIT_NUM = 1000;

	public List<Comment> getComment() {

        Connection connection = null;

        try {
            connection = getConnection();

            UserCommentDao userCommentDao = new UserCommentDao();

            List<Comment> ret = userCommentDao.getComments(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}

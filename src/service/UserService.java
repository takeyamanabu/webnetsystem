package service;


import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserDetail;
import dao.UserDao;
import dao.UserDetailDao;
import utils.CipherUtil;

public class UserService {

	//ユーザー新規登録
	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	//ユーザ情報を編集するメソッド

	 public void update(User user) {
		 Connection connection = null;
		 try {
	            connection = getConnection();

	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);

	            UserDao userDao = new UserDao();
	            userDao.update(connection, user);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	 }

	//アカウント再開停止を編集するメソッド

		 public void updateOnOff(User user) {
			 Connection connection = null;
			 try {
		            connection = getConnection();

		            UserDao userDao = new UserDao();
		            userDao.updateOnOff(connection, user);

		            commit(connection);
		        } catch (RuntimeException e) {
		            rollback(connection);
		            throw e;
		        } catch (Error e) {
		            rollback(connection);
		            throw e;
		        } finally {
		            close(connection);
		        }
		 }
	 //ユーザIDからユーザ情報を取り出す
	 public User getUser(int userId) {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        UserDao userDao = new UserDao();
		        User user = userDao.getUser(connection, userId);

		        commit(connection);

		        return user;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}

	 //ユーザ新規登録画面にてログインＩＤの重複の確認（ＤＡＯとの連絡）
	 public boolean getLoginId(String loginid){

		 Connection connection = null;
		 try {
		        connection = getConnection();

		        UserDao userDao = new UserDao();
		        Boolean checkLoginId = userDao.loginIdChecker(connection, loginid);

		        commit(connection);

		        return checkLoginId;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}

	//ユーザ新規登録画面にてログインＩＤの重複の確認（ＤＡＯとの連絡）
		 public boolean getEditLoginId(String loginid, int userid){

			 Connection connection = null;
			 try {
			        connection = getConnection();

			        UserDao userDao = new UserDao();
			        Boolean checkLoginId = userDao.editLoginIdChecker(connection, loginid, userid);

			        commit(connection);

			        return checkLoginId;
			    } catch (RuntimeException e) {
			        rollback(connection);
			        throw e;
			    } catch (Error e) {
			        rollback(connection);
			        throw e;
			    } finally {
			        close(connection);
			    }
			}


    //ユーザー情報一覧表示用に必要なデータをリストアップするメソッド

	private static final int LIMIT_NUM = 1000;

	public List<UserDetail> getDetail() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDetailDao detailDao = new UserDetailDao();
			List<UserDetail> ret = detailDao.getUserDetails(connection, LIMIT_NUM);

			commit(connection);

			return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}
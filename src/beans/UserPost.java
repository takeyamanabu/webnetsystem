package beans;

import java.io.Serializable;
import java.util.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int userId;
    private int editid;
	private String title;
    private String name;
    private String category;
	private String text;
    private Date created_date;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getEditid() {
		return editid;
	}
	public void setEditId(int editid) {
		this.editid = editid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	 public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}


}